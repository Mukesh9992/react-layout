import { TestBed, inject } from '@angular/core/testing';

import { LocaldataserviceService } from './localdataservice.service';

describe('LocaldataserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocaldataserviceService]
    });
  });

  it('should be created', inject([LocaldataserviceService], (service: LocaldataserviceService) => {
    expect(service).toBeTruthy();
  }));
});
