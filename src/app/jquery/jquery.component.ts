import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jquery',
  templateUrl: './jquery.component.html',
  styleUrls: ['./jquery.component.css']
})
export class JqueryComponent implements OnInit {

  constructor() { }
  ngOnInit() {
  }
  books: any[] = [
    {
      "name": "Jquery1",
      "price": 35,
      "author": 'Douglas Pace'
    },
    {
      "name": "Jquery2",
      "price": 32,
      "author": 'Mcleod Mueller'
    },
    {
      "name": "Jquery3",
      "price": 21,
      "author": 'Day  Meyers'
    },
    {
      "name": "Jquery4",
      "price": 34,
      "author": 'Aguirre  Ellis"'
    },
    {
      "name": "Jquery5",
      "price": 32,
      "author": 'Cook  Tyson'
    }
  ];
}
