import { Component,ViewEncapsulation,OnInit} from '@angular/core';
import { Router} from '@angular/router';
import 'rxjs/operators/debounceTime';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  logindata: any = {
    username: '',
    password: ''
  }
  isError:boolean=false;
  constructor(private router: Router) { }
  // Login Validation with username and Password
     loginData() { 
         if(this.logindata.username.length && this.logindata.password.length <= 8){
          this.router.navigate(['/main']);
             this.isError = false;
             } else {
             this.isError = true;
         }  
        console.log(this.loginData());
        }
         
   keyDownFunction(event) {
    if(event.keyCode == 13) {
        this.loginData();
   }
   }
   valuechange(data){
    Object.getOwnPropertyNames(data).forEach(item=>{
        this.logindata[item]=data[item];
    })
  }
  ngOnInit(){
  }
  }
