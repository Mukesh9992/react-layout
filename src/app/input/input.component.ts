import { Component, OnInit,EventEmitter,Input,Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() id:string;
  @Input() label:string;
  @Input() ivalue :string;
  @Input() type:string;
  @Output() ivaluechange = new EventEmitter(); 

  constructor() { }
  ngOnInit() {
  }
  change(event){
    var obj={};
    obj[event.currentTarget.id]=this.ivalue
    this.ivaluechange.emit(obj)
  }
}
