import { Component } from '@angular/core';

@Component({
  selector: 'app-javascript',
  templateUrl: './javascript.component.html',
  styleUrls: ['./javascript.component.css']
})
export class JavascriptComponent  {

  displayedColumns = ['Position', 'Name', 'Version','Author','Download'];
  dataSource = ELEMENT_DATA;

}
  export interface Element {
    name: string;
    position: number;
    version: number;
    author : string;
  }
  
  const ELEMENT_DATA: Element[] = [
    {position: 1, name: 'Javascript',author: 'Someone', version: 1.0079},
    {position: 2, name: 'Javascript',author: 'Someone' ,version: 4.0026 },
    {position: 3, name: 'Typescript',author : 'Someone' ,version: 6.941},
    {position: 4, name: 'Typescript',author : 'Someone', version: 9.0122},
    {position: 5, name: 'Coffeescript',author : 'Someone', version: 10.811},
  ];

