import { TestBed, inject } from '@angular/core/testing';

import { MylayoutService } from './mylayout.service';

describe('MylayoutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MylayoutService]
    });
  });

  it('should be created', inject([MylayoutService], (service: MylayoutService) => {
    expect(service).toBeTruthy();
  }));
});
