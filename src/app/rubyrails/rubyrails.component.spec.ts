import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RubyrailsComponent } from './rubyrails.component';

describe('RubyrailsComponent', () => {
  let component: RubyrailsComponent;
  let fixture: ComponentFixture<RubyrailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubyrailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RubyrailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
