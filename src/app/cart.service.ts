import { Injectable } from '@angular/core';

@Injectable()
export class CartService {

  
  tables=[
    {id : 1, name :   'Javascript Book',   price : 24},
    {id : 2, name :   'Angular Book',      price : 43},
    {id : 3, name :   'Git Book',          price : 12},
    {id : 4, name :   'Vuejs Book',        price : 13}
  ];
  constructor() { }

  getMyItems(){
    
  return this.tables;
}
}
