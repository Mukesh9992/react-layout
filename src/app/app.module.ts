import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './/app-routing.module';
import { MatDialogModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {GitService} from './git.service';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DemoMaterialModule } from './app.materialmodule';
import { Section1Component } from './section1/section1.component';
import { LoginComponent } from './login/login.component';
import { BlogComponent } from './blog/blog.component';
import { AngularComponent } from './angular/angular.component';
import { ReactComponent } from './react/react.component';
import { VuejsComponent } from './vuejs/vuejs.component';
import { JavascriptComponent } from './javascript/javascript.component';
import { FooterComponent } from './footer/footer.component';
import { JqueryComponent } from './jquery/jquery.component';
import { CssComponent } from './css/css.component';
import { HtmlComponent } from './html/html.component';
import { MylayoutService} from './mylayout.service';
import {LayoutService} from './layout.service'
import { DialogComponent } from './dialog/dialog.component';
import { DialogDataExample } from './dialog/dialog-data-example';
import { SingupComponent } from './singup/singup.component';
import { SassComponent } from './sass/sass.component';
import { LessComponent } from './less/less.component';
import { GitComponent } from './git/git.component';
import { RubyrailsComponent } from './rubyrails/rubyrails.component';
import { InputComponent } from './input/input.component';
import { CartComponent } from './cart/cart.component';
import { CartService } from './cart.service';
//import { FilterPipe} from './filter.pipe';
//import {FormsModule } from '@angular/forms'
//import { HighlightComponent } from './hightlight/highlight.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    Section1Component,
    LoginComponent,
    BlogComponent,
    AngularComponent,
    ReactComponent,
    VuejsComponent,
    JavascriptComponent,
    FooterComponent,
    JqueryComponent,
    CssComponent,
    HtmlComponent,
    DialogComponent,
    DialogDataExample,
    SingupComponent,
    SassComponent,
    LessComponent,
    GitComponent,
    RubyrailsComponent,
    InputComponent,
    CartComponent,
    //FilterPipe 
    //HighlightComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DemoMaterialModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MylayoutService,LayoutService,GitService,CartService],
  entryComponents: [
    DialogDataExample
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
