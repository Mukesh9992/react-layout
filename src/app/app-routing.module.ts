import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Section1Component} from './section1/section1.component';
import { BlogComponent} from './blog/blog.component';
import { LoginComponent} from './login/login.component';
import { AngularComponent} from './angular/angular.component';
import { ReactComponent} from './react/react.component';
import { VuejsComponent} from './vuejs/vuejs.component';
import { JavascriptComponent} from './javascript/javascript.component';
import {JqueryComponent} from './jquery/jquery.component';
import {CssComponent} from './css/css.component';
import {HtmlComponent} from './html/html.component';
import { SingupComponent } from './singup/singup.component';
import {LessComponent} from './less/less.component';
import {SassComponent} from './sass/sass.component';
import {CartComponent} from './cart/cart.component'
import {GitComponent} from './git/git.component';
import {RubyrailsComponent} from './rubyrails/rubyrails.component';

const routes: Routes = [

  //Navigation,Tabsection,Login Router,Othe Router Concepts

  {path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: 'main', component: MainComponent},
  {path: 'section1', component : Section1Component},
  {path : 'blog', component : BlogComponent}, 
  {path : 'login', component : LoginComponent},
  {path : 'angular', component : AngularComponent},
  {path : 'react',component : ReactComponent},
  {path : 'vuejs', component : VuejsComponent},
  {path : 'javascript' , component : JavascriptComponent},
  {path : 'jquery', component : JqueryComponent},
  {path : 'css',component : CssComponent},
  {path : 'html',component : HtmlComponent},
  {path : 'signup',component : SingupComponent},
  {path : 'less',component : LessComponent},
  {path : 'sass',component : SassComponent},
  {path : 'git',component : GitComponent},
  {path : 'rubyrails',component : RubyrailsComponent},
  {path: 'cart',component : CartComponent}
]

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forRoot(routes) ],
  declarations: []
})

export class AppRoutingModule { }