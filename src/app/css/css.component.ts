import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  templateUrl: './css.component.html',
  styleUrls: ['./css.component.css']
})

export class CssComponent implements OnInit {
  itemCount: number = 4;
  btnText: string = 'Add an Item';
  btnTest : string = 'Delete An Item'
  goalText: string = '';
  goals = [];
  //count : number = 0;
  isError:boolean=false;
  constructor() { }

  ngOnInit() {
    this.itemCount = this.goals.length;
  }
  keyDownFunction(event) {
    if(event.keyCode == 13) {
        this.addItem();
    }
   }
  addItem():void {
    if(this.goalText.length > 0){
      this.isError= false;
    }
    else{
      this.isError = true;
      this.itemCount = 0;
    }
    this.goals.push(this.goalText);
    this.goalText = '';
    this.itemCount = this.goals.length;
  }
  deleteItem(){
    this.goals.pop();
    this.itemCount = --this.itemCount;
  }
}
