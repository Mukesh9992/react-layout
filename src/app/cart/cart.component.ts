import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
// Cart 
export class CartComponent{
  color: string;
  id : number;
  name : string;
  constructor(private cartservice : CartService) {
   }
}
