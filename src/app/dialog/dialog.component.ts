import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {DialogDataExample} from './dialog-data-example';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  
    constructor(public dialog: MatDialog) {}

    openDialog() {
      const dialogRef = this.dialog.open(DialogDataExample, {
        height: '370px',
        width: '370px',
      });

      // Dialog Box Enter Result
      
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
      });
    }
  }

