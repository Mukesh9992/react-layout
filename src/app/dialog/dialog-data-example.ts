
import { Component,Inject} from '@angular/core';
import{MatDialog, MatDialogRef,MAT_DIALOG_DATA} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dialog-data-example',
  templateUrl: './dialog-data-example.html',
  styleUrls: ['./dialog-data-example.css']
})
export class DialogDataExample{

 constructor(public dialogRef: MatDialogRef<DialogDataExample>,@Inject(MAT_DIALOG_DATA) public data: any,private router: Router)
  {}
 
  onNoClick(): void {
    this.dialogRef.close();
}
// Dialog-data-example
}

