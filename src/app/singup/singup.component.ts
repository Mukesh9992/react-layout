import { Component, OnInit } from '@angular/core';
import 'rxjs/operators/debounceTime';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {

  //Specifying the Signup Form
    signupdata: any = { 
    firstname : '',
    lastname : '',
    email : '',
    password: ''
  }
  isError : boolean;
  isUsername : boolean;
  constructor() { }

 signupData(){
   console.log(this.signupdata);
   //Try this in Switch case You can get it.
   if(this.signupdata.email.length && this.signupdata.password.length > 8 ){
     this.isError = false;
   }else {
      this.isError = true;
     }
 }
 valuechange(data){
  Object.getOwnPropertyNames(data).forEach(item=>{
      this.signupdata[item]=data[item];
  })
}
  ngOnInit() {
  }

}
