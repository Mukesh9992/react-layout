import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of} from 'rxjs/observable/of';

@Injectable()

export class MylayoutService {

  constructor(private http : HttpClient) { }

  // Gettin the Signup Credentials

  getSignup(): Observable<any[]> {
    return this.http.get<any[]>('api/signupUrl')
  }

  // Getting the Login Credentials

  getLogin(): Observable<any[]> {
    return this.http.get<any[]>('api/loginUrl')
  }

  // Getting Data from Cart Component From Cart

  getCart() : Observable<any[]>{
    return this.http.get<any[]>('api/cartUrl')
  }
  // Getting Data from Git Component From Backend

  getGit() : Observable<any[]>{
    return this.http.get<any[]>('api/gitUrl')
  }
}
