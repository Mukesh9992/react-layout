import { Component, OnInit } from '@angular/core';
import { GitService } from '../git.service';

@Component({
  selector: 'app-git',
  templateUrl: './git.component.html',
  styleUrls: ['./git.component.css']
})
export class GitComponent{
  id : number;
  name : string;
  
 tables =[];
  constructor(private gitservice : GitService) {
    this.tables = gitservice.getMyItems();
   }
}
