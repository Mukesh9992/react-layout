import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
// Loginpage Without Navigation and Footer Part
export class AppComponent {
islogin:boolean;
 constructor(private route: ActivatedRoute, private router: Router) {
    this.router.events.subscribe((event) => {
      if (event.constructor.name === "NavigationEnd") {
        this.islogin =!(window.location.pathname.indexOf('login') >= 0 || window.location.pathname.length<=1);
      }
      else if(event.constructor.name === "FooterEnd"){
        this.islogin =!(window.location.pathname.indexOf('login') >= 0 || window.location.pathname.length<=1);
      }
    })
  }
}