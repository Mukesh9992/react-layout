import { Component} from '@angular/core';
import { MylayoutService } from '../mylayout.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent{
  title = 'Library';
  constructor(private router: Router) { }
  
}
