# Project :library Application

The Library Application.

## Purpose

The Main Purpose of the Application is, user can login in to their Application,once logged in to their Application. They Will get a Dashboard of Having Diff Kind of Options,User can Download the Books read the Blog & Articles written On various Technologies. 


## Adding the Angularjs

To Intall the Angular Globally,

If Already Installed Globally Skip the Next Step.

run `npm install -g @angular/cli` Commond In Cli

To Create a project 

Run `ng new my-app` commond In Cli.

cd  in to `my-app`.

## Adding Bootstrap

For Extraordinary Styling used Bootstrap 

Include the Bootstrap File through npm Commonds

run  `npm install bootstrap`

Include a File 

`../node_modules/bootstrap/dist/css/bootstrap.min.css` 

In .angular-cli.json

## Adding Angular Material

Ang Material used For layout of the dashboard and to show the list of Books

Install Angularjs Material From The Angular Material Site Through npm Commonds

run `npm install --save @angular/material @angular/cdk`

Save Css Styling In Global Css Folder  `../node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css`

## Adding the Angularjs Router

Purpose To Add : The Main Purpose of the Angularjs Router Is to Integrate the All Files In to Single Page Application Concept (SPA),without Page Reloading.

To add the Router Concept in to the project

run  `npm Install @angular/router` in the Commond In the Cli

## Layout

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
